const { declare } = require('@babel/helper-plugin-utils')
const deepMerge = require('deepmerge')

const defaultOptions = {
  env: undefined,
  transforms: {
    jsx: {
      runtime: 'automatic',
      importSource: 'preact',
    },
    runtime: {
      absoluteRuntime: true,
      helpers: true,
      corejs: 3,
      regenerator: true,
      useESModules: true,
    },
  },
}

const isObject = (obj) =>
  !!obj && typeof obj === 'object' && !Array.isArray(obj)

module.exports = declare((api, options) => {
  const { env, transforms } = deepMerge(defaultOptions, options)

  const presets = []
  if (isObject(env)) {
    presets.push(['@babel/env', env])
  }

  const plugins = ['@babel/plugin-syntax-dynamic-import']

  if (transforms.jsx) {
    plugins.push([
      '@babel/plugin-transform-react-jsx',
      isObject(transforms.jsx) ? transforms.jsx : defaultOptions.transforms.jsx,
    ])
  }
  if (transforms.runtime) {
    plugins.push([
      '@babel/plugin-transform-runtime',
      isObject(transforms.runtime)
        ? transforms.runtime
        : defaultOptions.transforms.runtime,
    ])
  }
  return {
    presets,
    plugins,
  }
})
