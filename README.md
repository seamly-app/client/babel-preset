## Seamly Babel-Preset

This is the default babel-preset for `@seamly/*` implementations.

## Installation

```
yarn add -D @seamly/babel-preset
// or
npm install -D @seamly/babel-preset
```

## Default Options

```
{
  env: undefined,
  transforms: {
    jsx: {
      runtime: 'automatic',
      importSource: 'preact',
    },
    runtime: {
      absoluteRuntime: true,
      helpers: true,
      corejs: 3,
      regenerator: true,
      useESModules: true,
    },
  },
}
```

### Presets

`@babel/preset-env` can be enabled by providing [preset options](https://babeljs.io/docs/en/babel-preset-env#options)

### Transforms

The following transforms are enabled by default, with default options, and can be disabled by providing `false` or modified by providing options which will be merged.

- [@babel/plugin-transform-react-jsx](https://babeljs.io/docs/en/babel-plugin-transform-react-jsx-compat)
- [@babel/plugin-transform-runtime](https://babeljs.io/docs/en/babel-plugin-transform-runtime)

#### Disabling transforms

Transforms can be disabled by providing `false`:

```
{
  transforms: {
    jsx: false
  }
}
```

#### Configuring options

Transforms can be configured with custom options by providing an object, which will be merged with the default options:

```
{
  transforms: {
    jsx: {
      importSource: 'react'
    }
  }
}
```

## Usage

This package can be used as `@seamly/babel-preset` following the Babel documentation on [Presets](https://babeljs.io/docs/en/options#presets).

### Within `babel.config.js` / `.babelrc.js`

```
module.exports = {
  ...
  presets: [
    '@seamly/babel-preset',
    ['@seamly/babel-preset', {
      // options
    }]
  ]
  ...
}
```

### Within `webpack.config.js`

```
  {
    loader: 'babel-loader',
    options: {
      presets: [
        '@seamly/babel-preset'
      ]
    }
  }
```
