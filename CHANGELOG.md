# Seamly Babel Preset Changelog

## [Unreleased]

## 1.2.0 (5 September 2024)

### Changed

- Updated dependencies (minor updates)

## 1.1.0 (2 February 2024)

### Changed

- Updated @babel plugins
- Updated @seamly/prettier-config to 3.1.0
- Updated @seamly/eslint-config to 3.1.0

## 1.0.0 (2 November 2023)

### Changed

- Updated @babel plugins
- Updated @seamly/prettier-config to 3.0.3
- Updated @seamly/eslint-config to 3.0.2
